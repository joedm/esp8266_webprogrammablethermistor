#include "NtpClient.h"

NtpClient ntp;

struct Time_t
{
   long Epoch;
   int Hour;
   int Minute;
   int Second;
   int SecondsSinceMidnight;
};

unsigned long _timeZoneOffsetSeconds;
unsigned long _epochTimeAtBoot;

class Time
{
  public:
    void SetTimeFromNtp() 
    {
      unsigned long currentEpoch = ntp.getEpochTime();
      unsigned long secondsSinceBoot = millis() / 1000UL;
      _epochTimeAtBoot = currentEpoch - secondsSinceBoot;
    };
    
    Time_t Now()
    {
      unsigned long now = EpochNow() + _timeZoneOffsetSeconds;
      Serial.println("epoch seconds now: " + String(now));
      Time_t rv = {
        now,
        (now  % 86400L) / 3600, // hours
        (now  % 3600) / 60, // minutes
        now % 60 }; // seconds
        rv.SecondsSinceMidnight = rv.Hour * 3600 + rv.Minute * 60 + rv.Second;
      return rv;
    };
    
    void SetTimeZone(int offsetInSeconds)
    {
      _timeZoneOffsetSeconds = offsetInSeconds;
    };

  private:
    unsigned long EpochNow()
    {
      unsigned long secondsSinceBoot = millis() / 1000UL;
      return _epochTimeAtBoot + secondsSinceBoot;
    };
};
