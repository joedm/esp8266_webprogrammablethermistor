#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <EEPROM.h>
#include <math.h>         //loads the more advanced math functions
#include "EEPROMAnything.h"
#include "JoesTime.h"

const char *ssid = "......";
const char *password = "......";
const int timeZoneOffsetSeconds = 36000; // +10:00 AEST in seconds.

MDNSResponder mdns;
ESP8266WebServer server(80);
Time myTime;

typedef class temperatureSetting_t
{
  public:
    double minTemp;
    double maxTemp;
};

#define intMax 2147483647
int currentState;
double latestTemp;
temperatureSetting_t  myTemperatures;

void SetConfigToDefault()
{
  myTemperatures.minTemp = 3.5;
  myTemperatures.maxTemp = 3.7;
}

void setup(void) {   
  pinMode(5, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  Serial.begin(115200);
  ConnectToWifi();
  SetupWebServer();
  
  myTime.SetTimeFromNtp();
  myTime.SetTimeZone(timeZoneOffsetSeconds);

  EEPROM.begin(512);
  ReadEep();
  currentState = HIGH;
  if (isnan(myTemperatures.minTemp))
  {
    SetConfigToDefault();  
  }

  digitalWrite(LED_BUILTIN, 1);
}

void ConnectToWifi()
{
  WiFi.hostname("fridge01");
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, 1);
    delay(250);
    Serial.print(".");
    digitalWrite(LED_BUILTIN, 0);
  }

  Serial.println("");
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void SetupWebServer()
{
  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/setup", HandleSetupRequest);

  server.on("/save", HandleSaveRequest);

  server.on("/read", HandleReadRequest);

  server.on("/currentState", HandleCurrentStateRequest);

  server.on("/factoryReset", HandleFactoryResetRequest);

  server.on("/time", HandleGetTimeRequest);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void handleRoot() {
  String html = "<b>Set the temperature range:</b><br />"
                "minTemp = The minimum temperature before the fan turns off"
                "maxTemp = The maximum temperature before the fan turns on"
                "<i>/setup&minTemp=3.5&maxTemp=3.8</i><br />"
                "<form action='/setup' method='GET'>"
                "Min: <input name='minTemp' value='3.5' size='10' /> "
                "Max: <input name='maxTemp' value='3.8' size='10' /> "
                "<input type='submit' value='Save' /> "
                "</form>"                
                "<br />"
                "<b>Save the current configuration to EEPROM</b><br />"
                "<i>/save</i><br />"
                "<form action='/save' method='GET'>"
                "<input type='submit' value='Save' /> "
                "</form>"
                "<br />"
                "<b>Read the configuration from EEPROM into current configuration</b><br />"
                "<i>/read</i><br />"
                "<form action='/read' method='GET'>"
                "<input type='submit' value='Read' /> "
                "</form>"
                "<br />"
                "<b>Display the current state</b><br />"
                "<i>/currentState</i><br />"
                "<form action='/currentState' method='GET'>"
                "<input type='submit' value='State' /> "
                "</form>"
                "<br />"
                "<b>Reset the current configuration to a default configuration</b><br />"
                "I'll level with you, This didn't really come from a factory.<br />"
                "<i>/factoryReset</i><br />"
                "<form action='/factoryReset' method='GET'>"
                "<input type='submit' value='Reset' /> "
                "</form>"  ;
  server.send(200, "text/html", html);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void loop(void) {
  server.handleClient();
  DoToggle();

  if (millis() % 360000 == 0)
  {
      myTime.SetTimeFromNtp();
  }
}

double getTemperature()
{
  int val;                //Create an integer variable
  double temp;            //Variable to hold a temperature value
  val=analogRead(A0);      //Read the analog port 0 and store the value in val
  temp=Thermister(val);   //Runs the fancy math on the raw analog value
  return temp;
}

double Thermister(int RawADC) 
{  //Function to perform the fancy math of the Steinhart-Hart equation
  double Temp;
  Temp = log(((10240000/RawADC) - 10000));
  Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
  Temp = Temp - 273.15;              // Convert Kelvin to Celsius
  // Temp = (Temp * 9.0)/ 5.0 + 32.0; // Celsius to Fahrenheit - comment out this line if you need Celsius
  return Temp;
}

void DoToggle()
{
  if (millis() % 1000 == 0)
  {
    double currentTemperature = getTemperature();
    latestTemp = currentTemperature;
    bool isOutsideTargetRange = currentTemperature > myTemperatures.maxTemp || currentTemperature < myTemperatures.minTemp;
    if (isOutsideTargetRange && currentTemperature > myTemperatures.maxTemp)
    {
      currentState = HIGH;
    }
    else if (isOutsideTargetRange && currentTemperature < myTemperatures.minTemp)
    {
      currentState = LOW;
    }

    digitalWrite(5, currentState);
    Serial.println("Pin: 5 set to: " + String(currentState));
    delay(1); // make sure we don't run multiple times within a single millisecond.
  }
}

void HandleCurrentStateRequest()
{
  String message = "{\n"
                   "  \"currentMillis\":" + String(millis()) + ",\n"
                   "  \"currentState\":" + String(currentState) + ",\n"
                   "  \"latestTemperature\":" + String(latestTemp) + ",\n"
                   "  \"minTemp\":" + String(myTemperatures.minTemp) + ",\n"
                   "  \"maxTemp\":" + String(myTemperatures.maxTemp) + "\n"
                   "\n}";
  server.send(200, "application/json", message);
}

void HandleSetupRequest() {
  if (server.arg("minTemp") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("minTemp", "minTemp is mandatory"));
    return;
  }

  if (server.arg("maxTemp") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("maxTemp", "maxTemp is mandatory"));
    return;
  }

  myTemperatures.minTemp = atof(server.arg("minTemp").c_str());
  myTemperatures.maxTemp = atof(server.arg("maxTemp").c_str());
  
  String message = "{\n"
                   "  \"success\":true"
                   "\n}";              
  server.send(200, "application/json", message);
}

void HandleFactoryResetRequest()
{
  if (server.arg("sure") == "yes")
  {
    SetConfigToDefault();
    server.send(200, "text/plain", "Current settings have been reset to default. \n"
                "If you want to keep these settings please call /save\n"
                "You can still get your old settings back by restarting without calling /save\n"
                "I know we're not making this easy, but it's a feature not a bug :P"
               );
  }
  else
  {
    server.send(200, "text/plain", "This will clear all your setting and reset them back to default.\n"
                "Default setting will set each pin to cycle every 5 minutes.\n"
                "If you're sure you want to reset then call this method again with the below: \n"
                "/factoryReset?sure=yes"
               );
  }
}

void HandleSaveRequest()
{
  SaveRunningConfig();
  String message = "{\n"
                   "  \"success\":true\n"
                   "}";
  server.send(200, "application/json", message);
}

void HandleReadRequest()
{
  ReadEep();
  String message = "{\n"
                   "  \"success\":true\n"
                   "}";
  server.send(200, "application/json", message);
}

void SaveRunningConfig()
{
  EEPROM_writeAnything(0, myTemperatures);
  EEPROM.commit();
  Serial.println("called write eeprom");
}

void ReadEep()
{
  EEPROM_readAnything(0, myTemperatures);
  Serial.println("called eeprom_read_block");
}

String GenerateMissingParameterJson(String field, String message)
{
  return "{\n"
         "  \"success\":false,\n"
         "  \"errors\":[\n"
         "    {\n"
         "      \"parameter\":\"" + field + "\",\n"
         "      \"message\":\"" + message + "\"\n"
         "    }\n"
         "  ]\n"
         "}";
}

void HandleGetTimeRequest()
{
  Time_t now = myTime.Now();
  String msg = "{\n"
  "  epoch:" + String(now.Epoch) + ",\n"
  "  hour:" + String(now.Hour) + ",\n"   
  "  minute:" + String(now.Minute) + ",\n"
  "  second:" + String(now.Second) + ",\n"
  "  secondsSinceMidnight:" + String(now.SecondsSinceMidnight) + "\n"
  "}";
  server.send(200, "application/json", msg);
}
